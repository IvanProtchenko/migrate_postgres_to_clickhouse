#!/bin/bash
PATCH_TO='/tmp'
PG_USER='postgres'
PG_DB='zabbix'
CL_USER='default'
CL_PASS='password'
CL_DB='zabbix'
sudo -u $PG_USER psql $PG_DB -c "COPY (SELECT to_char(to_timestamp(clock) AT TIME ZONE 'UTC-3', 'YYYY-MM-DD') as day, itemid, clock, value, ns FROM history) TO '$PATCH_TO/history.csv' CSV;"
sudo -u $PG_USER psql $PG_DB -c "COPY (SELECT to_char(to_timestamp(clock) AT TIME ZONE 'UTC-3', 'YYYY-MM-DD') as day, itemid, clock, value, ns FROM history_uint) TO '$PATCH_TO/history_uint.csv' CSV;"
sudo -u $PG_USER psql $PG_DB -c "COPY (SELECT to_char(to_timestamp(clock) AT TIME ZONE 'UTC-3', 'YYYY-MM-DD') as day, itemid, clock, value, ns FROM history_str) TO '$PATCH_TO/history_str.csv' CSV;"
sudo -u $PG_USER psql $PG_DB -c "COPY (SELECT to_char(to_timestamp(clock) AT TIME ZONE 'UTC-3', 'YYYY-MM-DD') as day, itemid, clock, value, ns FROM history_text) TO '$PATCH_TO/history_text.csv' CSV;"
sudo -u $PG_USER psql $PG_DB -c "COPY (SELECT to_char(to_timestamp(clock) AT TIME ZONE 'UTC-3', 'YYYY-MM-DD') as day, itemid, clock, value, ns FROM history_log) TO '$PATCH_TO/history_log.csv' CSV;"
clickhouse-client --user $CL_USER --password $CL_PASS --database $CL_DB --format_csv_delimiter="," --query="INSERT INTO history (day, itemid, clock,value_dbl, ns) FORMAT CSV" < $PATCH_TO/history.csv
clickhouse-client --user $CL_USER --password $CL_PASS --database $CL_DB --format_csv_delimiter="," --query="INSERT INTO history (day, itemid, clock,value, ns) FORMAT CSV" < $PATCH_TO/history_uint.csv
clickhouse-client --user $CL_USER --password $CL_PASS --database $CL_DB --format_csv_delimiter="," --query="INSERT INTO history (day, itemid, clock,value_str, ns) FORMAT CSV" < $PATCH_TO/history_str.csv
clickhouse-client --user $CL_USER --password $CL_PASS --database $CL_DB --format_csv_delimiter="," --query="INSERT INTO history (day, itemid, clock,value_str, ns) FORMAT CSV" < $PATCH_TO/history_text.csv
clickhouse-client --user $CL_USER --password $CL_PASS --database $CL_DB --format_csv_delimiter="," --query="INSERT INTO history (day, itemid, clock,value_str, ns) FORMAT CSV" < $PATCH_TO/history_log.csv

